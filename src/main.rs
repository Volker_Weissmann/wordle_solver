use indicatif::ProgressBar;
use rayon::prelude::*;
use std::io::BufRead;

const WORD_LENGTH: usize = 5;

const CORRECT_POSITION: u8 = 0;
const WRONG_LETTER: u8 = 1;
const WRONG_POSITION: u8 = 2;
const INVALID: u8 = 3;

fn predict_feedback(
    guess: &[char; WORD_LENGTH],
    solution: &[char; WORD_LENGTH],
) -> [u8; WORD_LENGTH] {
    let mut ret = [WRONG_LETTER; WORD_LENGTH];
    for i in 0..WORD_LENGTH {
        if guess[i] == solution[i] {
            ret[i] = CORRECT_POSITION;
        } else if solution.contains(&guess[i]) {
            ret[i] = WRONG_POSITION;
        }
    }
    ret
}

fn fast_entropy_of_word(
    possible_words: &[[char; WORD_LENGTH]],
    guess: &[char; WORD_LENGTH],
) -> f64 {
    let mut counts = [0; 3_usize.pow(WORD_LENGTH as u32)];
    possible_words.iter().for_each(|solution| {
        let bin = ternary_to_binary(predict_feedback(guess, solution));
        counts[bin] += 1;
    });
    counts
        .iter()
        .map(|&count| prob_to_entropy(count as f64 / possible_words.len() as f64))
        .sum()
}

// Returns 1 if wordle will color the guess `guess` according to `ternary`, if
// the correct solution that wordle has in mind is `solution`. Returns 0
// otherwise.
fn ishit(
    guess: &[char; WORD_LENGTH],
    ternary: &[u8; WORD_LENGTH],
    solution: &[char; WORD_LENGTH],
) -> usize {
    if (0..WORD_LENGTH).any(|i| ternary[i] == CORRECT_POSITION && guess[i] != solution[i]) {
        return 0;
    }
    if (0..WORD_LENGTH).any(|i| ternary[i] == WRONG_LETTER && solution.contains(&guess[i])) {
        return 0;
    }
    if (0..WORD_LENGTH).any(|i| {
        ternary[i] == WRONG_POSITION && (!solution.contains(&guess[i]) || guess[i] == solution[i])
    }) {
        return 0;
    }
    1
}

fn num_matching(
    possible_words: &[[char; WORD_LENGTH]],
    guess: &[char; WORD_LENGTH],
    ternary: &[u8; WORD_LENGTH],
) -> usize {
    possible_words
        .iter()
        .map(|solution| ishit(guess, ternary, solution))
        .sum()
}

fn binary_to_ternary(binary: usize) -> [u8; WORD_LENGTH] {
    // Explanation of the `ternary` variable:
    // 0 = correct letter, correct position
    // 1 = wrong letter, wrong position
    // 2 = correct letter, wrong position
    // (see CORRECT_POSITION, WRONG_LETTER, WRONG_POSITION)
    let mut temp = binary;
    let mut ternary = [INVALID; WORD_LENGTH];
    for i in 0..WORD_LENGTH {
        ternary[i] = (temp % 3) as u8;
        temp /= 3;
    }
    // Alternative implementation: Makes clippy happy, but is worse
    // let ternary: [usize; WORD_LENGTH] = (0..WORD_LENGTH)
    //     .map(|_| {
    //         let r = temp % 3;
    //         temp /= 3;
    //         r
    //     })
    //     .collect::<Vec<_>>()
    //     .try_into()
    //     .unwrap();
    ternary
}

fn ternary_to_binary(ternary: [u8; WORD_LENGTH]) -> usize {
    ternary
        .iter()
        .enumerate()
        .map(|(i, &trit)| trit as usize * 3_usize.pow(i as u32))
        .sum::<usize>()
}

fn prob_to_entropy(prob: f64) -> f64 {
    if prob == 0. {
        0.
    } else {
        -prob * prob.ln()
    }
}

fn slow_entropy_of_word(
    possible_words: &[[char; WORD_LENGTH]],
    guess: &[char; WORD_LENGTH],
) -> f64 {
    let mut entropy = 0.;
    for binary in 0..(3_usize.pow(WORD_LENGTH as u32)) {
        let n = num_matching(possible_words, guess, &binary_to_ternary(binary));
        entropy += prob_to_entropy(n as f64 / possible_words.len() as f64);
    }
    entropy
}

fn main() {
    // Todo: add support for entering uppercase letters
    let wrong_letters: Vec<char> = vec![];
    let wrong_position: [Vec<char>; WORD_LENGTH] = [vec![], vec![], vec![], vec![], vec![]];
    let correct_positions: [Option<char>; WORD_LENGTH] = [None, None, None, None, None];

    // https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt
    let file = match std::fs::File::open("words_alpha.txt") {
        Ok(file) => file,
        Err(err) => {
            if err.kind() == std::io::ErrorKind::NotFound {
                panic!("please run: wget https://raw.githubusercontent.com/dwyl/english-words/master/words_alpha.txt")
            } else {
                panic!("Unable to open words_alpha.txt: {}", err);
            }
        }
    };
    let mut all_words = vec![];
    let mut possible_words = vec![];
    for word in std::io::BufReader::new(file).lines() {
        let word = word.unwrap();
        if word.len() != WORD_LENGTH {
            continue;
        }
        let word: [char; WORD_LENGTH] = word
            .to_lowercase()
            .chars()
            .collect::<Vec<_>>()
            .try_into()
            .unwrap();
        all_words.push(word);
        if wrong_letters.iter().any(|c| word.contains(c)) {
            continue;
        }
        if word
            .iter()
            .zip(correct_positions)
            .any(|(&guess, real)| match real {
                Some(real) => guess != real,
                None => false,
            })
        {
            continue;
        }
        if word.iter().zip(&wrong_position).any(|(&guess, real)| {
            real.iter()
                .any(|&real| guess == real || !word.contains(&real))
        }) {
            continue;
        }
        possible_words.push(word);
    }
    println!("{} possible word(s) remaining", possible_words.len());
    if possible_words.len() < 20 {
        possible_words.iter().for_each(|word| {
            println!(
                "{} {}",
                word.iter().collect::<String>(),
                fast_entropy_of_word(&possible_words, word)
            )
        });
    }

    let bar = ProgressBar::new(all_words.len() as u64);
    let entropies = all_words
        .par_iter()
        .map(|guess| {
            bar.inc(1);
            fast_entropy_of_word(&possible_words, guess)
        })
        .collect::<Vec<f64>>();
    bar.finish();

    let mut indices = (0..all_words.len()).collect::<Vec<_>>();
    indices.sort_by(|&i, &j| entropies[i].partial_cmp(&entropies[j]).unwrap());
    for i in 0..20 {
        let ind = indices[indices.len() - i - 1];
        println!(
            "{} {}",
            all_words[ind].iter().collect::<String>(),
            entropies[ind]
        );
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_binary_ternary() {
        for binary in 0..20 {
            let roundtrip = ternary_to_binary(binary_to_ternary(binary));
            assert_eq!(binary, roundtrip);
        }
    }
}
